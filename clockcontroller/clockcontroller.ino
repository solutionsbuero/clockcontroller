
/********
 * Config
 ********/

// Delay between clicks, regulates over-all speed
int loop_delay = 300;

// Coil charge-up time, regulates impulse of a click
int charge_delay = 170;

// Coil discharge time, should be small to prevent impulse in wrong direction
int discharge_delay = 10;

// Break between charge and discharge, keep small to keep accuraccy
int switching_delay = 5;

/********
 * End Config
 ********/


// Pinout
byte output_a1 = 2;
byte output_a2 = 3;


void setup() {
  pinMode(output_a1, OUTPUT);
  pinMode(output_a2, OUTPUT);
  neutral();
}

void coil_charge() {
  digitalWrite(output_a1, LOW);
  digitalWrite(output_a2, HIGH);
}

void coil_discharge() {
  digitalWrite(output_a1, HIGH);
  digitalWrite(output_a2, LOW);
}

void neutral() {
  digitalWrite(output_a1, LOW);
  digitalWrite(output_a2, LOW);
}

void loop() {
  coil_charge();
  delay(charge_delay);
  neutral();
  delay(switching_delay);
  coil_discharge();
  delay(discharge_delay);
  neutral();
  delay(loop_delay);
}
